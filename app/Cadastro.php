<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB, DateTime;

class Cadastro extends Model
{
    protected $table = 'cadastro';

    public $timestamps = false;
    
    protected $fillable = [ 'nome', 'cpf', 'rg', 'telefone' ];

    public function cadastroByOrdem( $get = '' )
    {
        $db = DB::table('cadastro')
                ->select('nome'
                         , 'cpf'
                         , 'rg'
                         , 'telefone'
                         , DB::raw('DATE_FORMAT(data_cadastro, \'%d/%m/%Y %h:%i:%s\') AS dataCadastroFormatada')
                         , DB::raw('DATE_FORMAT(data_nascimento, \'%d/%m/%Y\') AS dataNascimentoFormatada'));

        if( !empty($get) && $get['data_cadastro'] ) {
			$date = DateTime::createFromFormat('d/m/Y', $get['data_cadastro']);
            $mysql_date_string = $date->format('Y-m-d');
            $db->whereRaw("DATE_FORMAT(data_cadastro, '%Y-%m-%d') = '".$mysql_date_string."'");	
        }

        if( !empty($get) && $get['data_nascimento'] ) {
			$date = DateTime::createFromFormat('d/m/Y', $get['data_nascimento']);
            $mysql_date_string = $date->format('Y-m-d');
            $db->where('data_nascimento', '=', $mysql_date_string);        		
        }

        if( !empty($get) && $get['nome'] ) {
            $value = $get['nome'];
            $db->where('nome', 'LIKE', "%$value%");
        }

        $db->orderBy('data_cadastro', 'DESC');

        return $db->get();
    }
}

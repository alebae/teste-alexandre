<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Input, View, Redirect;
use App\Cadastro, App\Estado;
use App\Http\Requests\CadastroRequest;

class CadastroController extends Controller
{
    private $model;
    private $modelUf;

    public function __construct() {
        $this->model = new Cadastro();                
        $this->modelUf = new Estado(); 
    }

    public function getIndex() {       
        $this->getUf();
        //dd(\Session::get('uf'));
        $list = $this->model->cadastroByOrdem($_GET); 

        $listUf = $this->modelUf->orderBy('sigla')->get();       
            
        return view('index', compact('list', 'listUf'));
    }

    public function getCadastrar() {   
        return view('cadastrar');
    }

    public function postStore(CadastroRequest $request) {
        $input = $request->all();
        
        return $this->doSave($input);
    }

    public function doSave($input) {
        
        if(!$this->validaIdade($input['data_nascimento'])) {
            return Redirect::to('/cadastrar')->with('message', 'Idade inválida! Abaixo de 18 anos!');
        } else {
            $class = new $this->model;
            $class->fill($input);
            $class->data_nascimento = \Carbon\Carbon::createFromFormat('d/m/Y', $input['data_nascimento'])->format('Y-m-d');
            $class->save();

            return Redirect::to('/')->with('mensagem', 'Cadastro realizado com sucesso!');
        }      
        
    }

    private function validaIdade($data) {
        if(\Session::get('uf') === 'PR' AND $this->retornaIdade($data) < 18) {
            return false;
        } else {
            return true;
        }
    }

    private function retornaIdade($data) {
        list($dia, $mes, $ano) = explode('/', $data);
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
   
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

        return $idade;
    }

    public function getUf($sigla = '') {
        if( !$sigla ) {
            $this->selecionaEstado();            
        }
        else {
            \Session::put('uf', $sigla);
        }

        return redirect('/');
    }

    private function selecionaEstado(){
        if(!\Session::get('uf')){
            \Session::put('uf', $this->modelUf->orderBy('sigla')->first()->sigla);
        }
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {       
        return [
            'nome'              => 'required',
            'cpf'               => 'required',
            'data_nascimento'   => 'date_format:d/m/Y',
            'telefone'          => 'required',
            'rg'                => 'required_with:uf'
        ];
    }
 
    public function messages()
    {
        return [
            'nome.required'                     => 'O campo nome é obrigatório.',
            'cpf.required'                      => 'O campo CPF é obrigatório.',
            'data_nascimento.date_format'       => 'O campo data de nascimento é obrigatório.',
            'telefone.required'                 => 'O campo telefone é obrigatório.',
            'rg.required_with'                  => 'O campo RG é obrigatório.'
        ];
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/


Route::get('/', 'CadastroController@getIndex');
Route::get('/uf/{sigla}', 'CadastroController@getUf');
Route::get('/cadastrar', 'CadastroController@getCadastrar');
Route::post('/store', 'CadastroController@postStore');

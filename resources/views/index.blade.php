<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Teste Alexandre</title>

        <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css')}}" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">   

        <link rel="stylesheet" href="{{asset('assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"> 


        <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery.maskedinput/dist/jquery.maskedinput.min.js')}}"></script>
        <script src="{{asset('assets/vendor/moment/moment.js')}}"></script>
        <script src="{{asset('assets/vendor/moment/locale/pt.js')}}"></script>
        <script src="{{asset('assets/vendor/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js')}}"></script>

        <script type="text/javascript">
            $(function(){
                 $(".dtNasc, .dtCad").mask("99/99/9999");

                 $('#datetimepickerNasc, #datetimepickerCad').datetimepicker({
                    locale: 'pt',
                    format: 'DD/MM/YYYY'
                 });
            });
        </script>


    </head>
    <body>
        <div class="">
            {!! Session::get('mensagem') ?  '<div class="alert alert-success">'.\Session::get('mensagem').'</div>' : '' !!}

            {!! Form::open(['id' => 'FormFiltrar', 'url' => '/', 'method' => 'GET']) !!}
            <div class="alert alert-default container" style="border:1px solid #efefef;padding:5px;">

                <div class="form-inline center-block">
                    <h4><i class="glyphicon glyphicon-filter"></i> Filtros</h4>
                    <div class="input-i">
                        <div class="col-group text-center">
                            <div class="col-xs-4 form-group">    
                                {!! Form::text('nome',  isset($_GET) && \Input::has('nome') ? \Input::get('nome') : '', ['class'=> 'form-control', 'placeholder' => 'Nome', 'size' => 50]  ) !!}
                            </div>
                            <div class="col-xs-3 form-group">    
                                <div class="input-group date" id="datetimepickerCad">
                                    {!! Form::text('data_cadastro', isset($_GET) && \Input::has('data_cadastro') ? \Input::get('data_cadastro') : '', ['class'=> 'form-control dtCad', 'placeholder' => 'Data de cadastro', 'size' => 30]) !!}
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span> 
                                </div>    
                            </div>
                            <div class="col-xs-3 form-group">   
                                <div class="input-group date" id="datetimepickerNasc"> 
                                {!! Form::text('data_nascimento', isset($_GET) && \Input::has('data_nascimento') ? \Input::get('data_nascimento') : '', ['class'=> 'form-control dtNasc', 'placeholder' => 'Data de nascimento', 'size' => 30] ) !!}
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span> 
                                </div>    
                            </div>
                            <div class="col-xs-2 form-group">
                                {!! Form::submit('Buscar', array('class'=> 'btn btn-success')) !!}
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            {!! Form::close() !!}

                <div class="container" style="margin-bottom:5px;">
                    <div class="pull-left">
                        <a href="{{url('/cadastrar')}}" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Cadastrar</a>
                    </div>

                    @if(count($listUf))
                        <div class="pull-right">
                            Estados:
                            @foreach($listUf as $uf)
                                <a href="{{url('uf/'.$uf->sigla)}}" class="btn {!! ((\Session::get('uf') === $uf->sigla) ? 'btn-primary' : 'btn-default') !!}">{{ $uf->sigla }}</a>
                            @endforeach
                        </div>
                    @endif

                </div>

                <div class="container">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>                            
                                <td>Data de Cadastro</td>
                                <td>Nome</td>
                                <td>CPF</td>
                                <td>RG</td>
                                <td>Data de nascimento</td>
                                <td>Telefone(s)</td>
                            </tr> 
                        </thead>
                        <tbody>
                            @if(count($list))
                                @foreach($list as $data)
                                    <tr>                                    
                                        <td>{{ $data->dataCadastroFormatada }}</td>
                                        <td>{!! $data->nome !!}</td>
                                        <td>{{ $data->cpf }}</td>
                                        <td>{{ $data->rg ? $data->rg : '-' }}</td>                                  
                                        <td>{{ $data->dataNascimentoFormatada }}</td>
                                        <td>{{ $data->telefone }}</td>  
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="6" align="center">Nenhum registro encontrado.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>

        </div>
    </body>
</html>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Teste Alexandre</title>

        <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/dist/css/bootstrap.min.css')}}" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">   

        <link rel="stylesheet" href="{{asset('assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"> 

        <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery.maskedinput/dist/jquery.maskedinput.min.js')}}"></script>
        <script src="{{asset('assets/vendor/moment/moment.js')}}"></script>
        <script src="{{asset('assets/vendor/moment/locale/pt.js')}}"></script>
        <script src="{{asset('assets/vendor/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js')}}"></script>

        <script type="text/javascript">
            $(function(){
                 $("#data_nascimento").mask("99/99/9999");
                 $("#cpf").mask("999.999.999-99");

                 $('#datetimepicker').datetimepicker({
                    locale: 'pt',
                    format: 'DD/MM/YYYY'
                 });
            });
        </script>
       
    </head>
    <body>
            @if(count($errors) || \Session::get('message'))
                <div class="alert alert-danger">
                    <ul>                         
                        @foreach($errors->all() as $message) 
                            <li>{!! $message !!}</li> 
                        @endforeach
                        {!! \Session::get('message') ? '<li>'.\Session::get('message').'</li>' : '' !!}
                    </ul> 
                </div>  
            @endif

            {!! Form::open(['url' => '/store', 'files' => true]) !!}
            <div class="container"> 
                    <h3 class="text-center"><i class="glyphicon glyphicon-file"></i> Cadastrar</h3>
                    <div class="form-group">
                        {!! Form::label('nome', 'Nome:') !!}
                        {!! Form::text('nome', '', ['class'=> 'form-control']  ) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('cpf', 'CPF:') !!}
                        {!! Form::text('cpf', '', ['class'=> 'form-control']  ) !!}
                    </div>

                    @if( \Session::get('uf') === 'SC' )
                        <div class="form-group">
                           {!! Form::label('rg', 'RG:') !!}
                           {!! Form::text('rg', '', ['class'=> 'form-control']  ) !!}
                        </div>                
                    @endif

                    <div class="form-group">
                        {!! Form::label('data_nascimento', 'Data de nascimento:') !!}
                        <div class="input-group date" id="datetimepicker">
                            {!! Form::text('data_nascimento', '', ['class'=> 'form-control']  ) !!} 
                           <span class="input-group-addon">
                              <span class="glyphicon glyphicon-calendar"></span>
                           </span> 
                        </div>                 
                    </div>
                    <div class="form-group">
                        {!! Form::label('telefone', 'Telefone(s):') !!}
                        {!! Form::textarea('telefone', '', ['class'=> 'form-control'] ) !!}
                    </div>
                
            </div>

            <div class="container">
                <div class="text-center">  
                    @if(\Session::get('uf') === 'SC')
                        {!! Form::hidden('uf', \Session::get('uf')) !!}
                    @endif
                    {!! Form::submit('Salvar', ['class'=> 'btn btn-success']) !!}
                    <a href="{{url('/')}}" class="btn btn-warning">Voltar</a>
                </div>
            </div>
            {!! Form::close() !!}
    </body>
</html>

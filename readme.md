#Prova Técnica - Alexandre Feustel Baehr    

Requisitos:  
1) Composer;  
2) NodeJS + NPM + Bower;  
3) PHP >=5.6.4;  
4) MySQL 5.5.  

Ferramentas/frameworks utilizados:  
1) Laravel 5.4 (https://laravel.com/docs/5.4);  
2) MySQL 5.5;  
4) JQuery;  
5) CSS/Bootstrap.    

Instruções:    

1) Clonar o projeto  
```bash
$ git clone https://alebae@bitbucket.org/alebae/teste-alexandre.git  
```

2) Navegar até a pasta criada  
```bash
$ cd teste-alexandre  
```

3) Executar o Composer   
```bash
$ composer install 
``` 

4) Executar o Bower  
```bash
$ bower install  
``` 

6) Crie um banco de dados  
```sql
CREATE DATABASE `teste-alexandrebaehr` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
```   

5) Rodar as migrations para gerar as tabelas do banco de dados  
```bash
$ php artisan migrate  
``` 

6) Rodar uma inserção de registros para estados  
```bash
$ php artisan db:seed 
``` 

7) Habilitar o servidor embutido no PHP.  
```bash
$ php artisan serve  
```